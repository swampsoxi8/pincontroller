//
//  MainViewController.swift
//  ATPinController
//
//  Created by Aaron Tredrea on 01/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit

class MainViewController: UIViewController,ATPinControllerDelegate {


    //MARK: - Outlets
    
    @IBOutlet weak var buttonView: UIView!{
        didSet{
            buttonView.layer.cornerRadius = 15
            buttonView.layer.masksToBounds = true
        }
    }

    //MARK: - Actions

    @IBAction func addPincode(_ sender: Any) {
        loadPinController(withState: .EnteringFirstPinForSetUp)
    }

    @IBAction func testPincode(_ sender: Any) {
        loadPinController(withState: .EnteringPinForAuthentication)
    }

    @IBAction func changePincode(_ sender: Any) {
        loadPinController(withState: .EnteringPinToChangeCurrentPIN)
    }


    /// Present ATPinController
    private func loadPinController(withState state:ATPinControllerState){
        let pinVC = ATPinController()
        pinVC.pinCodeViewControllerState = state
        pinVC.delegate = self
        pinVC.modalPresentationStyle = .overCurrentContext
        pinVC.modalTransitionStyle = .coverVertical
        present(pinVC, animated: true, completion: nil)
    }


    //MARK: - ATPinControllerDelegate

    /// Do what you like with succsess
    func pinSuccess(){
        
    }
    
    /// Vildation failed
    func pinFailure(){
        
    }
}
