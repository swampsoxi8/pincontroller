//
//  ATPinController.swift
//  ATPinController
//
//  Created by Aaron Tredrea on 01/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit

/// Enum to track controller state
enum ATPinControllerState {
    case EnteringFirstPinForSetUp
    case EnteringSecondPinForSetup
    case EnteringPinForAuthentication
    case EnteringPinToChangeCurrentPIN
}

/// Protocol to call back to presenting controller
protocol ATPinControllerDelegate:class {
    func pinSuccess()
    func pinFailure()
}

class ATPinController: UIViewController {


    //MARK:- Data

    /// Key to store passcode to `UserDefaults`
    let userDeafaultsPasscodeKey = "UseYourOwnCustomKey"


    /// Strings
    struct ATPinCodeString {
        static let reEnterPasscode = "Re-enter Passcode"
        static let passcodeNotMatched = "Passcodes do not match"
        static let enterNewPasscode = "Enter new passcode"
        static let enterPasscode = "Enter passcode"
    }


    //MARK: - Outlets

    /// Main title label
    @IBOutlet weak var titleLabel: UILabel!
    /// UI to load selected numbers
    @IBOutlet weak var oneNumberSelected: UIButton!
    @IBOutlet weak var twoNumberSelected: UIButton!
    @IBOutlet weak var threeNumberSelected: UIButton!
    @IBOutlet weak var fourNumberSelected: UIButton!
    /// View to hold buttons
    @IBOutlet weak var selectedButtonsCollectionView: UIView!
    /// Collection to reference buttons
    @IBOutlet var selectedButtonsCollection: [UIButton]!
    @IBOutlet weak var containingView: UIView!


    //MARK:- Properties

    weak var delegate:ATPinControllerDelegate?
    /// ATPinControllerState property to manage state of controller
    var pinCodeViewControllerState:ATPinControllerState = .EnteringPinForAuthentication
    /// Array to hold pincode
    var savedPinArray = [Int]()
    /// Array to store numbers as they are selected or deleted
    private var pinArray = [Int](){didSet{

        // Remove all selections
        deselectAllPinSelections()

        // Dont proceeed unless the array has valuse
        if !pinArray.isEmpty{
            // Loop through array and use `int` to use as a value for getting the buttons tag
            for int in  1...pinArray.count{

                for button in selectedButtonsCollection{
                    // If one of the buttons in the collecion has this tag the show it as `selected`
                    if button.tag == int{
                        button.isSelected = true
                    }
                }
            }

            // If the array has all four numbers the proceed to checking/saving the passcode
            if pinArray.count == 4{
                checkPin()
            }
        }

        }
    }

    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Only allow portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }

    //MARK:- Private functions

    private func shakePinSelectionButtons(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.1
        animation.repeatCount = 2
        animation.autoreverses = true
        let fromCGPoint = CGPoint(x: selectedButtonsCollectionView.center.x - 50, y: selectedButtonsCollectionView.center.y)
        let toCGPoint = CGPoint(x: selectedButtonsCollectionView.center.x + 50, y: selectedButtonsCollectionView.center.y)
        animation.fromValue = NSValue(cgPoint: fromCGPoint)
        animation.toValue =  NSValue(cgPoint: toCGPoint)
        selectedButtonsCollectionView.layer.add(animation, forKey: "position")
    }


    private func checkPin(){

        switch pinCodeViewControllerState {
        case .EnteringFirstPinForSetUp:
            savedPinArray = pinArray
            pinArray.removeAll()
            deselectAllPinSelections()
            titleLabel.text = ATPinCodeString.reEnterPasscode
            pinCodeViewControllerState = .EnteringSecondPinForSetup
            break
        case .EnteringSecondPinForSetup:
            if savedPinArray == pinArray{
                savePassword()
            }else{
                titleLabel.text = ATPinCodeString.passcodeNotMatched
                shakePinSelectionButtons()
                pinArray.removeAll()
                deselectAllPinSelections()
            }
            break
        case .EnteringPinForAuthentication:
            if pinIsCorrect(){
                pinServicesSuccess()
            }else{
                shakePinSelectionButtons()
                pinArray.removeAll()
                deselectAllPinSelections()
            }
        case .EnteringPinToChangeCurrentPIN:

            if userHasPin(){
                if pinIsCorrect(){
                    pinCodeViewControllerState = .EnteringFirstPinForSetUp
                    pinArray.removeAll()
                    deselectAllPinSelections()
                    titleLabel.text = ATPinCodeString.enterNewPasscode
                }else{
                    shakePinSelectionButtons()
                    pinArray.removeAll()
                    deselectAllPinSelections()
                }
            }else{
                savedPinArray = pinArray
                pinArray.removeAll()
                deselectAllPinSelections()
                titleLabel.text = ATPinCodeString.reEnterPasscode
                pinCodeViewControllerState = .EnteringSecondPinForSetup
            }
        }
    }

    private func deselectAllPinSelections(){
        for button in selectedButtonsCollection{
            button.isSelected = false
        }
    }

    private func pinIsCorrect() -> Bool{
        guard let pinData = UserDefaults.standard.data(forKey: userDeafaultsPasscodeKey) else {
            return false
        }
        guard let array = NSKeyedUnarchiver.unarchiveObject(with: pinData) as? [Int] else{
            return false
        }

        return array == pinArray
    }

    private func userHasPin() -> Bool{

        guard let pinData = UserDefaults.standard.data(forKey: userDeafaultsPasscodeKey) else {
            return false
        }
        guard let _ = NSKeyedUnarchiver.unarchiveObject(with: pinData) as? [Int] else{
            return false
        }

        return true
    }

    private func savePassword(){
        let password = NSKeyedArchiver.archivedData(withRootObject: pinArray)
        UserDefaults.standard.set(password, forKey: userDeafaultsPasscodeKey)
        pinServicesSuccess()
    }

    private func pinServicesFailed(){
        dismiss(animated: true) {
            self.delegate?.pinFailure()
        }
    }
    private func pinServicesSuccess(){
        dismiss(animated: true) {
            self.delegate?.pinSuccess()
        }
    }


    //MARK:- Actions

    @IBAction func numberSelected(_ sender: UIButton) {

        let digitAsString = String(sender.tag)

        guard let digit = Int(digitAsString) else{
            return
        }
        pinArray.append(digit)
    }

    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        if pinArray.count > 0{
            pinArray.removeLast()
        }
    }
}


